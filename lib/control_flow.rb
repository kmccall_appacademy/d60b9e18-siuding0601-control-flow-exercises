# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.each_char do |ch|
    next if ch == ch.upcase
      str.delete!(ch)
  end
  str
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  if str.length.odd?
    str[(str.split("").length)/2]
  else
    str[str.split("").length/2 - 1 .. str.split("").length/2]
  end
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  count = 0
  str.each_char do |el|
    if VOWELS.include?(el)
      count += 1
    end
  end
  count
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  ans = 1
  num.downto(1) do |n|
    ans *= n
  end
  ans
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  ans = ""
  arr.each do |el|
    ans << el + separator
  end
  if separator == ""
    ans
  else
    ans.chop
  end
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  ans = ""
  str.downcase.split("").each_with_index do |al, n|
    if (n+1).odd?
      ans << al.downcase
    else
      ans << al.upcase
    end
  end
    ans
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  words = str.split(" ")
  ans  = []
  words.each do |w|
    if w.length > 4
      ans << w.reverse
    else
      ans << w
    end
  end
    ans.join(" ")
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  ans =[]
  n.downto(1) do |num|
    if num % 15 == 0
      ans << "fizzbuzz"
    elsif num % 3 == 0
      ans << "fizz"
    elsif num % 5 == 0
      ans <<  "buzz"
    else
      ans << num
    end
  end
  ans.reverse
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  ans = []
  arr.each_index do |n|
    ans << arr [arr.length - n - 1]
  end
  ans
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  count = 0
  if num == 1
    return false
  end
  num.downto(1).each do |n|
    if num % n == 0
      count += 1
    end
  end
  if count > 2
    return false
  else
    return true
  end
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  ans =[]
  num.downto(1).each do |n|
    if num % n == 0
      ans << n
    end
  end
  ans.reverse
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  factor = factors(num)
  ans = []
  factor.each do |n|
    if prime?(n)
      ans << n
    end
  end
  ans
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).count
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  oddcount = []
  evencount = []
  arr.each do |num|
    if num.odd?
      oddcount << num
    else
      evencount << num
    end
  end
  if oddcount.count < evencount.count
    oddcount.last
  else
    evencount.last
  end
end
